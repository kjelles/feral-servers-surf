CATEGORY_NAME = "Community Links"

// Apply
function ulx.apply(ply)
	ply:SendLua([[gui.OpenURL("https://fx-gaming.enjin.com/forum/m/33692348/viewforum/7612800")]])
end
local apply = ulx.command( CATEGORY_NAME, "ulx apply", ulx.apply, "!apply" )
apply:defaultAccess( ULib.ACCESS_ALL )
apply:help( "The forum for surf applications." )

// Forums
function ulx.website(ply)
	ply:SendLua([[gui.OpenURL("https://fx-gaming.enjin.com/")]])
end
local website = ulx.command( CATEGORY_NAME, "ulx website", ulx.website, "!website" )
website:defaultAccess( ULib.ACCESS_ALL )
website:help( "Displays our wonderful website." )

// Rules
function ulx.rules(ply)
	ply:SendLua([[gui.OpenURL("https://fx-gaming.enjin.com/forum/m/33692348/viewthread/30101409-official-surf-rules")]])
end
local rules = ulx.command( CATEGORY_NAME, "ulx rules", ulx.rules, "!rules" )
rules:defaultAccess( ULib.ACCESS_ALL )
rules:help( "Displays our rules." )

// Discord
function ulx.discord(ply)
	ply:SendLua([[gui.OpenURL("https://discordapp.com/invite/tZMKStz")]])
end
local discord = ulx.command( CATEGORY_NAME, "ulx discord", ulx.discord, "!discord" )
discord:defaultAccess( ULib.ACCESS_ALL )
discord:help( "Links you to our discord." )

// Twitter
function ulx.twitter(ply)
	ply:SendLua([[gui.OpenURL("http://twitter.com/fxgaminguk/")]])
end
local twitter = ulx.command( CATEGORY_NAME, "ulx twitter", ulx.twitter, "!twitter" )
twitter:defaultAccess( ULib.ACCESS_ALL )
twitter:help( "Follow the official Nugget Mix Gaming Twitter account ." )

// Donate
function ulx.donate(ply)
	ply:SendLua([[gui.OpenURL("http://nuggetmixdonations.nn.pe/store.php?page=packages&id=9")]])
end
local donate = ulx.command( CATEGORY_NAME, "ulx donate", ulx.donate, "!donate" )
donate:defaultAccess( ULib.ACCESS_ALL )
donate:help( "Our donation store." )