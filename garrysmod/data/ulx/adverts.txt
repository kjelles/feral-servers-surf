; Here's where you put advertisements
;
; Whether an advertisement is a center advertisement (csay) or text box advertisement (tsay) is determined by
; whether or not the "time_on_screen" key is present. If it is present, it's a csay.
;
; The 'time' argument inside a center advertisement and the number following a chat advertisement are the
; time it takes between each showing of this advertisement in seconds. Set it to 300 and the advertisement
; will show every five minutes.
;
; If you want to make it so that one advertisement is shown and then will always be followed by another,
; put them in a table. For example, if you add the following to the bottom of the file, A will always show
; first followed by B.
; "my_group"
; {
;     {
;         "text" "Advertisement A"
;         "time" "200"
;     }
;     {
;         "text" "Advertisement B"
;         "time" "300"
;     }
; }


{
	"text" "Join us on Teamspeak: ts91.gameservers.com:9167"
	"red" "220"
	"green" "0"
	"blue" "255"
	"time" "450"
}
{
	"text" "Please read the rules if you don't know them via !rules"
	"red" "255"
	"green" "0"
	"blue" "0"
	"time" "330"
}
{
	"text" "New to surf? Type !tutorial"
	"red" "0"
	"green" "246"
	"blue" "255"
	"time" "470"
}
{
	"text" "We have finally published VIP, Check it out via !donate"
	"red" "0"
	"green" "255"
	"blue" "29"
	"time" "300"
}
{
	"text" "Want a knife? !unbox it or join our steamgroup by typing !steam"
	"red" "220"
	"green" "0"
	"blue" "255"
	"time" "200"
}
{
	"text" "Check out your knives by typing !knives"
	"red" "0"
	"green" "255"
	"blue" "29"
	"time" "225"
}
{
	"text" "Please report any bugs on the forums!"
	"red" "255"
	"green" "0"
	"blue" "0"
	"time" "300"
}
