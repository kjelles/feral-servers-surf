surface.CreateFont( "AdminHeader", { font = "Roboto Thin", size = 48, weight = 100 } )

surface.CreateFont( "AdminBase", { font = "Roboto Thin", size = 32, weight = 200 } )

surface.CreateFont( "AdminList", { font = "Segoe UI Light", size = 19, weight = 200 } )

surface.CreateFont( "AdminInfo", { font = "Segoe UI Light", size = 42, weight = 200 } )

surface.CreateFont( "AdminRanks", { font = "Lane - Narrow", size = 32, weight = 100 } )

local meta = FindMetaTable( "Panel" )

function meta:SpreadOpen( w, h, t )
	self:SetSize( 0, 0 )
	self:SetPos( ScrW() / 2 - w / 2, ScrH() / 2 - h / 2 )
	self:SetAlpha( 0 )
	self:SizeTo( w, 5, t, 0, 1, function() self:SizeTo( w, h, t ) end )
	self:MoveTo( ScrW() / 2 - w / 2, ScrH() / 2 - h / 2, t )
	self:AlphaTo( 255, t )
end

function meta:SpreadClosed( t )
	self:SetSize( self:GetWide(), self:GetTall() )
	self:SetPos( ScrW() / 2 - self:GetWide() / 2, ScrH() / 2 - self:GetTall() / 2 )
	self:SetAlpha( 255 )
	self:SizeTo( self:GetWide(), 5, t, 0, 1, function() self:SizeTo( 0, 5, t, 0, 1, function() self:Remove()  end ) end )
	self:AlphaTo( 0, t, t ) 
end

function string.UpperFirst( str )
	local tbl = {}
	local s = string.Explode( "", str )
	s[ 1 ] = s[ 1 ]:upper()
	tbl = table.concat( s, "" )
	return tbl
end
