local cols = {
	white = Color( 230, 230, 230 ),
	dwhite = Color( 210, 210, 210 ),
	gray = Color( 195, 195, 195 ),
	dgray = Color( 45, 45, 45 ),
	lgray = Color( 195, 195, 195, 180 ),
	fgray = Color( 195, 195, 195, 120 ),
	rfgray = Color( 195, 195, 195, 60 )
}

local function CreateAdminStats()
	local ply = LocalPlayer()
	
	local data  = net.ReadTable()
	local allranks = net.ReadTable()
	local rankperms = net.ReadTable()
	
	local ranks = {}
		
	for id, info in pairs( data ) do
		if !table.HasValue( ranks, info.group ) then
			table.insert( ranks, info.group )
		end
	end
	
	table.sort( ranks )
	table.sort( allranks )
	
	local currank = ranks[ 1 ]
	local curranktbl = {}
	
	local curuser = 1
	local curonline = false
	
	local serverrank = ""
	local highestrank = ""
	local lowestrank = ""
	local averageplayers = ""
	
	http.Fetch( "http://www.gametracker.com/server_info/" .. AdminStats.ServerIP, function( b )
		local unused, endrank = b:find( "Percentile</span>)" )
		local unused, startrank = b:find( "Game Server Rank:</span>" )
		
		if !endrank then
			serverrank = "Unknown"
			highestrank = "Unknown"
			lowestrank = "Unknown"
			averageplayers = "Unknown"
			return
		end
		
		serverrank = b:sub( startrank, endrank ):Replace( "<span>", "" ):Replace( "</span>", "" ):Replace( ">", "" ):Trim() -- welp, this is long isn't it now?
		
		local endmixed = b:find( [[<div class="block630_content_right">]] )
		
		local startmixed = b:find( '<span class="item_color_title">H' )
		local cleanedup = b:sub( startmixed, endmixed - 1 ):Replace( '<span class="item_color_title">', "" ):Replace( "Highest (past month):", "" ):Replace( "Lowest (past month):", "" ):Replace( "</span>", "" ):Trim() -- well then...
		
		local endhighest = cleanedup:find( "&nbsp;" )
		
		highestrank = cleanedup:sub( 1, endhighest - 1 ):Trim()
		lowestrank = cleanedup:sub( endhighest + 6, endmixed ):Replace( "</div>", "" ):Trim()
		
		local unused, averageplayersstart = b:find( 'id="HTML_avg_players"' )
		local averageplayersend = b:find( "</span>", averageplayersstart )
		
		averageplayers = b:sub( averageplayersstart + 2, averageplayersend - 1 ):Trim()
	end )

	local AdminStatsFrame = vgui.Create( "DFrame" )
	AdminStatsFrame:SpreadOpen( 1280, 720, AdminStats.AnimationTime ) -- Smallest 16:9 ratio. If someone is on smaller, they need to get out.
	AdminStatsFrame:SetTitle( "" )
	AdminStatsFrame:ShowCloseButton( false )
	AdminStatsFrame:SetDraggable( false )
	AdminStatsFrame:MakePopup()
	AdminStatsFrame.Paint = function( s, w, h )
		Derma_DrawBackgroundBlur( s )
		draw.RoundedBox( 0, 0, 0, w, h, cols.white )
		draw.SimpleText( "Staff Statistics", "AdminHeader", w / 2 + w / 5 / 2, 32, cols.dgray, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		
		draw.RoundedBox( 0, 0, 64, w, 1, cols.gray )
	end
	
	local AdminStatsSide = vgui.Create( "DFrame", AdminStatsFrame )
	AdminStatsSide:SetSize( 256, 720 )
	AdminStatsSide:SetPos( 0 )
	AdminStatsSide:SetTitle( "" )
	AdminStatsSide:ShowCloseButton( false )
	AdminStatsSide:SetDraggable( false )
	AdminStatsSide.Paint = function( s, w, h )
		draw.RoundedBox( 0, 0, 0, w, h, cols.dgray )
		draw.SimpleText( "- " .. ply:Nick(), "AdminBase", 52 + 10, 30, cols.gray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
		
		draw.RoundedBox( 0, 0, 64, w, 1, cols.white )
	end
	
	local AdminStatsAvatar = vgui.Create( "AvatarCircleMask", AdminStatsFrame )
	AdminStatsAvatar:SetSize( 52, 52 )
	AdminStatsAvatar:SetPos( 5, 5 )
	AdminStatsAvatar:SetPlayer( ply:SteamID64(), 52 )
	AdminStatsAvatar:SetMaskSize( 26 )
	
	timer.Simple( AdminStats.AnimationTime * 2 + .01, function()
		local at = AdminStats.AnimationTime == 0 and .01 or AdminStats.AnimationTime / 2
		
		local CloseButton = vgui.Create( "DButton", AdminStatsFrame )
		CloseButton:SetColor( Color( 255, 255, 255 ) )
		CloseButton:SetText( "CLOSE" )
		CloseButton:SetSize( 60, 23 )
		CloseButton:SetAlpha( 0 )
		CloseButton:AlphaTo( 255, at )
		CloseButton:SetPos( AdminStatsFrame:GetWide() - CloseButton:GetWide() - 5, 0 )
		CloseButton.DoClick = function()
			AdminStatsFrame:SpreadClosed( AdminStats.AnimationTime )
		end
		CloseButton.Paint = function( s, w, h )
			local col = Color( 190, 90, 90 )
					
			if s.Hovered then
				col = Color( 190, 40, 40 )
			end

			draw.RoundedBox( 0, 0, 0, w, h, col )
		end
		
		local AdminRankList = vgui.Create( "DListView", AdminStatsFrame )
		AdminRankList:SetSize( 1280 - 256 - 20, 80 )
		AdminRankList:SetPos( 256 + 10, 64 + 10 + 30 )
		AdminRankList:SetMultiSelect( false )
		AdminRankList:SetAlpha( 0 )
		AdminRankList:AlphaTo( 255, at, at * 2.5  )
		
		AdminRankList.Name = AdminRankList:AddColumn( "Name" )
		AdminRankList.SteamID = AdminRankList:AddColumn( "SteamID" )
		AdminRankList.Rank = AdminRankList:AddColumn( "Rank" )
		
		AdminRankList.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.dwhite )
		end
		
		local bar = AdminRankList.VBar
		bar.Paint = function() end
		
		bar.btnUp.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 80 ) )
		end
		
		bar.btnDown.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 80 ) )
		end
		
		bar.btnGrip.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 120 ) )
		end
		
		AdminRankList.Name.Header:SetDisabled( false )
		AdminRankList.Name.Header:SetTextColor( cols.gray )
		AdminRankList.Name.Header:SetFont( "AdminList" )
		AdminRankList.Name.Header.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.rfgray )
			draw.RoundedBox( 0, 0, 0, w - 1, h, cols.dgray )  
		end
		
		AdminRankList.SteamID.Header:SetDisabled( false )
		AdminRankList.SteamID.Header:SetTextColor( cols.gray )
		AdminRankList.SteamID.Header:SetFont( "AdminList" )
		AdminRankList.SteamID.Header.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.rfgray )
			draw.RoundedBox( 0, 0, 0, w, h, cols.dgray )  
		end
		
		AdminRankList.Rank.Header:SetDisabled( false )
		AdminRankList.Rank.Header:SetTextColor( cols.gray )
		AdminRankList.Rank.Header:SetFont( "AdminList" )
		AdminRankList.Rank.Header.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.rfgray )
			draw.RoundedBox( 0, 1, 0, w - 1, h, cols.dgray )  
		end
		
		for id, info in pairs( data ) do
			if info.group == currank then
				table.insert( curranktbl, { name = info.name == nil and "Unknown" or info.name, steamid = id, group = info.group, allow = info.allow, deny = info.deny, firstjoin = info.firstjoin, lastjoin = info.lastjoin, totaltime = info.totaltime, totalkicks = info.totalkicks, totalbans = info.totalbans, timeperiod = info.timeperiod, timeperiodold = info.timeperiodold } )
				nline = AdminRankList:AddLine( info.name == nil and "Unknown" or info.name, id, info.group )
				
				for i = 1, 3 do
					nline.Columns[ i ]:SetTextColor( Color( 10, 10, 10 ) )
					nline.Columns[ i ]:SetFont( "AdminList" )
				end
			end
		end
		
		for _, ply in pairs( player.GetAll() ) do
			if ply:SteamID() == curranktbl[ curuser ].steamid then
				curonline = true
				break
			end
		end
		
		local AdminStatsPanel = vgui.Create( "DFrame", AdminStatsFrame )
		AdminStatsPanel:SetSize( 1280 - 256 - 20, 720 )
		AdminStatsPanel:SetPos( 256 + 10, 64 + 10 + 30 + 80 + 10 )
		AdminStatsPanel:SetAlpha( 0 )
		AdminStatsPanel:AlphaTo( 255, at, at * 3 )
		AdminStatsPanel:SetTitle( "" )
		AdminStatsPanel:ShowCloseButton( false )
		AdminStatsPanel:SetDraggable( false )
		AdminStatsPanel.Paint = function( s, w, h )
			local online = curonline and "Yes" or "No"
			
			draw.SimpleText( "Statistics for " .. curranktbl[ curuser ].steamid, "AdminHeader", w / 2, 20, cols.dgray, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "- " .. curranktbl[ curuser ].name, "AdminInfo", 10 + 64, 51 + 10 + 30, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
		
			draw.SimpleText( "SteamID64: " .. util.SteamIDTo64( curranktbl[ curuser ].steamid ), "AdminInfo", 1, 51 + 10 + 30 + 32 + 20, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			--draw.SimpleText( "Current Rank: " .. curranktbl[ curuser ].group:UpperFirst(), "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 40, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER ) -- Replacing this but keeping it just incase.
		
			draw.SimpleText( "Total Kicks/Bans: " .. curranktbl[ curuser ].totalkicks .. "/" .. curranktbl[ curuser ].totalbans, "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 40, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Total Playtime: " .. curranktbl[ curuser ].totaltime, "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 80, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Weekly Playtime: " .. curranktbl[ curuser ].timeperiod .. " (" .. curranktbl[ curuser ].timeperiodold .. " last)", "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 120, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "First Join: " .. curranktbl[ curuser ].firstjoin, "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 160, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Last Join: " .. curranktbl[ curuser ].lastjoin, "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 200, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Online: " .. online, "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 240, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Allow: ", "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 280, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Deny: ", "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 320, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Set Group: ", "AdminInfo", 1, 51 + 10 + 30 + 32 + 20 + 360, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.RoundedBox( 0, 0, 51, w, 1, cols.gray )
			
			-- I can be messy sometimes, heh... SPLIT IN CENTER HERE
			local admintbl = {}
			
			for _, admin in pairs( player.GetAll() ) do
				if admin:IsAdmin() then
					table.insert( admintbl, admin )
				end
			end
			
			draw.RoundedBox( 0, w / 2, 51, 1, h, cols.gray )
			
			draw.SimpleText( "Current Server Information", "AdminInfo", w / 2 + w / 4, 51 + 10 + 30, cols.dgray, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Host: " .. GetHostName(), "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Gamemode: " .. engine.ActiveGamemode():UpperFirst(), "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 40, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Map: " .. game.GetMap(), "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 80, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Player Count: " .. #player.GetAll() .. "/" .. game.MaxPlayers(), "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 120, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Admin Count: " .. #admintbl, "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 160, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Server Rank: " .. serverrank, "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 200, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Highest Rank: " .. highestrank .. " (Past Month)", "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 240, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Lowest Rank: " .. lowestrank .. " (Past Month)", "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 280, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Average Players: " .. averageplayers .. " (Past Month)", "AdminInfo", 1 + 10 + w / 2, 51 + 10 + 30 + 32 + 20 + 320, cols.dgray, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
		end

		local AdminStatsPersonAvatar = vgui.Create( "AvatarCircleMask", AdminStatsFrame )
		AdminStatsPersonAvatar:SetSize( 64, 64 )
		AdminStatsPersonAvatar:SetPos( 256 + 10, 64 + 10 + 30 + 80 + 64 - 2 + 10 )
		AdminStatsPersonAvatar:SetAlpha( 0 )
		AdminStatsPersonAvatar:AlphaTo( 255, at, at * 3 )
		AdminStatsPersonAvatar:SetPlayer( util.SteamIDTo64( curranktbl[ curuser ].steamid ), 64 )
		AdminStatsPersonAvatar:SetMaskSize( 32 )
		
		AdminRankList.DoDoubleClick = function( lid, line )
			curuser = line
			AdminStatsPersonAvatar:SetPlayer( util.SteamIDTo64( curranktbl[ curuser ].steamid ), 64 )
			
			for _, ply in pairs( player.GetAll() ) do
				if ply:SteamID() == curranktbl[ curuser ].steamid then
					curonline = true
					break
				else
					curonline = false
				end
			end
		end
		
		local AdminStatsAllowed = vgui.Create( "DButton", AdminStatsFrame )
		AdminStatsAllowed:SetSize( 100, 30 )
		AdminStatsAllowed:SetPos( 256 + 10 + 80, 51 + 10 + 30 + 32 + 20 + 240 + 64 + 10 + 30 + 80 - 1 + 40 )
		AdminStatsAllowed:SetAlpha( 0 )
		AdminStatsAllowed:AlphaTo( 255, at, at * 3 )
		AdminStatsAllowed:SetText( "View Allowed" )
		AdminStatsAllowed:SetTextColor( Color( 10, 10, 10 ) )
		AdminStatsAllowed:SetFont( "AdminList" )
		AdminStatsAllowed.Paint = function( s, w, h )
			local col = s.Hovered and cols.dwhite or cols.gray
			draw.RoundedBox( 0, 0, 0, w - 1, h, col )  
		end
		AdminStatsAllowed.DoClick = function()
			local menu = DermaMenu()

			for _, perm in pairs( curranktbl[ curuser ].allow ) do
				menu:AddOption( _ ):SetIcon( "icon16/add.png" )
				menu:AddSpacer()
			end
			
			menu:Open()
		end
				
		local AdminStatsDenied = vgui.Create( "DButton", AdminStatsFrame )
		AdminStatsDenied:SetSize( 100, 30 )
		AdminStatsDenied:SetPos( 256 + 10 + 80, 51 + 10 + 30 + 32 + 20 + 280 + 64 + 10 + 30 + 80 - 1 + 40 )
		AdminStatsDenied:SetAlpha( 0 )
		AdminStatsDenied:AlphaTo( 255, at, at * 3 )
		AdminStatsDenied:SetText( "View Denied" )
		AdminStatsDenied:SetTextColor( Color( 10, 10, 10 ) )
		AdminStatsDenied:SetFont( "AdminList" )
		AdminStatsDenied.Paint = function( s, w, h )
			local col = s.Hovered and cols.dwhite or cols.gray
			draw.RoundedBox( 0, 0, 0, w - 1, h, col )  
		end
		AdminStatsDenied.DoClick = function()
			local menu = DermaMenu()

			for _, perm in pairs( curranktbl[ curuser ].deny ) do
				menu:AddOption( _ ):SetIcon( "icon16/delete.png" )
				menu:AddSpacer()
			end
			
			menu:Open()
		end
		
		local AdminStatsSetRank = vgui.Create( "DButton", AdminStatsFrame )
		AdminStatsSetRank:SetSize( 100, 30 )
		AdminStatsSetRank:SetPos( 256 + 10 + 80 + 60 + 2, 51 + 10 + 30 + 32 + 20 + 320 + 64 + 10 + 30 + 80 - 1 + 40 )
		AdminStatsSetRank:SetAlpha( 0 )
		AdminStatsSetRank:AlphaTo( 255, at, at * 3 )
		AdminStatsSetRank:SetText( "Set Rank" )
		AdminStatsSetRank:SetTextColor( Color( 10, 10, 10 ) )
		AdminStatsSetRank:SetFont( "AdminList" )
		AdminStatsSetRank.Paint = function( s, w, h )
			local col = s.Hovered and cols.dwhite or cols.gray
			draw.RoundedBox( 0, 0, 0, w - 1, h, col )  
		end
		AdminStatsSetRank.DoClick = function()
			local menu = DermaMenu()
			
			for _, rank in pairs( allranks ) do
				menu:AddOption( rank, function()
					net.Start( "AdminStatsChangeRank" )
					net.WriteString( rank )
					net.WriteString( curranktbl[ curuser ].steamid )
					net.SendToServer()
				end ):SetIcon( "icon16/status_online.png" )
				menu:AddSpacer()
			end
			
			menu:Open()
		end
				
		local AdminStatsProfile = vgui.Create( "DButton", AdminStatsFrame )
		AdminStatsProfile:SetColor( Color( 255, 255, 255 ) )
		AdminStatsProfile:SetText( "" )
		AdminStatsProfile:SetSize( 64, 64 )
		AdminStatsProfile:SetAlpha( 0 )
		AdminStatsProfile:AlphaTo( 255, at )
		AdminStatsProfile:SetPos( 256 + 10, 64 + 10 + 30 + 80 + 64 - 2 + 10 )
		AdminStatsProfile.DoClick = function()
			gui.OpenURL( "http://steamcommunity.com/profiles/" .. util.SteamIDTo64( curranktbl[ curuser ].steamid ) )
		end
		AdminStatsProfile.Paint = function( s, w, h )
		end
		
		local AdminRankSearch = vgui.Create( "DTextEntry", AdminStatsFrame )
		AdminRankSearch:SetSize( ( 1280 - 256 - 20 ) / 3, 20 )
		AdminRankSearch:SetPos( 256 + 10, 64 + 10 )
		AdminRankSearch:SetText( "Search" )
		AdminRankSearch:SetAlpha( 0 )
		AdminRankSearch:AlphaTo( 255, at, at * 2  )
		AdminRankSearch:SetTextColor( cols.dgray )
		AdminRankSearch:SetFont( "AdminList" )
		AdminRankSearch.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.dgray )
			draw.RoundedBox( 0, 1, 1, w - 2, h - 2, cols.dwhite )
			s:DrawTextEntryText( cols.dgray, Color( 110, 180, 250 ), cols.dgray )
		end
		
		-- Bringing the hacky over from Staff Time tracker, whoop
		
		local linesfilled = true
		local peopleamt = {}
		
		AdminRankSearch.OnEnter = function( text )
			if text:GetText() == "" then return end
			
			for _, info in pairs( curranktbl ) do
				if !info.name:lower():find( text:GetText():lower(), 1, false ) then
					linesfilled = false
					if _ > #AdminRankList.Lines then return end
					
					AdminRankList.Lines[ _ ]:Hide()
				else
					table.insert( peopleamt, _ )
					if _ > #AdminRankList.Lines then return end
					local u = 76561198220671407
					for i = 1, #peopleamt do
						AdminRankList.Lines[ _ ]:SetPos( 1, ( i * 17 ) - 17 )
					end
				end
			end
		end
		
		AdminRankSearch.OnChange = function( text )
			if text:GetText() == "" and !linesfilled then
				table.Empty( peopleamt )
				
				for i = 1, #AdminRankList.Lines do
					AdminRankList:RemoveLine( i )
				end
				
				for id, info in pairs( data ) do
					if info.group == currank then
						table.insert( curranktbl, { name = info.name == nil and "Unknown" or info.name, steamid = id, group = info.group, allow = info.allow, deny = info.deny, firstjoin = info.firstjoin, lastjoin = info.lastjoin, totaltime = info.totaltime, totalkicks = info.totalkicks, totalbans = info.totalbans, timeperiod = info.timeperiod, timeperiodold = info.timeperiodold } )
						nline = AdminRankList:AddLine( info.name == nil and "Unknown" or info.name, id, info.group )
						
						for i = 1, 3 do
							nline.Columns[ i ]:SetTextColor( Color( 10, 10, 10 ) )
							nline.Columns[ i ]:SetFont( "AdminList" )
						end
						
						linesfilled = true
					end
				end
			end
		end
		
		local AdminRankPerms = vgui.Create( "DFrame", AdminStatsFrame )
		AdminRankPerms:SetSize( AdminStatsFrame:GetWide() / 2, AdminStatsFrame:GetTall() )
		AdminRankPerms:Center()
		AdminRankPerms:SetDraggable( false )
		AdminRankPerms:ShowCloseButton( false )
		AdminRankPerms:SetVisible( false )
		AdminRankPerms.Paint = function( s, w, h )
		Derma_DrawBackgroundBlur( s )
			draw.RoundedBox( 0, 0, 0, w, h, cols.white )
			draw.SimpleText( "Rank Permissions", "AdminHeader", w / 2, 32, cols.dgray, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.RoundedBox( 0, 0, 64, w, 1, cols.gray )
		end
		
		local PermsCloseButton = vgui.Create( "DButton", AdminRankPerms )
		PermsCloseButton:SetColor( Color( 255, 255, 255 ) )
		PermsCloseButton:SetText( "CLOSE" )
		PermsCloseButton:SetSize( 60, 23 )
		PermsCloseButton:SetPos( AdminRankPerms:GetWide() - PermsCloseButton:GetWide() - 5, 0 )
		PermsCloseButton.DoClick = function()
			AdminRankPerms:SetVisible( false )
		end
		PermsCloseButton.Paint = function( s, w, h )
			local col = Color( 190, 90, 90 )
					
			if s.Hovered then
				col = Color( 190, 40, 40 )
			end

			draw.RoundedBox( 0, 0, 0, w, h, col )
		end
		
		local AdminPermList = vgui.Create( "DListView", AdminRankPerms )
		AdminPermList:SetSize( AdminRankPerms:GetWide(), AdminRankPerms:GetTall() - 64 )
		AdminPermList:SetPos( 0, 65 )
		AdminPermList:SetMultiSelect( false )
		AdminPermList.Perm = AdminPermList:AddColumn( "Permission" )
		
		AdminPermList.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.dwhite )
		end
		
		local bar = AdminPermList.VBar
		bar.Paint = function() end
		
		bar.btnUp.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 80 ) )
		end
		
		bar.btnDown.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 80 ) )
		end
		
		bar.btnGrip.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 120 ) )
		end
		
		AdminPermList.Perm.Header:SetDisabled( false )
		AdminPermList.Perm.Header:SetTextColor( cols.gray )
		AdminPermList.Perm.Header:SetFont( "AdminList" )
		AdminPermList.Perm.Header.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, cols.rfgray )
			draw.RoundedBox( 0, 0, 0, w, h, cols.dgray )  
		end
		
		for _, perm in pairs( rankperms[ currank ] ) do
			if type( perm ) == "table" then
				for _, perms in pairs( perm ) do
					AdminPermList:AddLine( perms )
				end
			end
		end
		
		local AdminRankScroll = vgui.Create( "DScrollPanel", AdminStatsSide )
		AdminRankScroll:SetSize( AdminStatsSide:GetWide(), AdminStatsSide:GetTall() - 64 )
		AdminRankScroll:SetPos( 0, 64 + 1 )
		
		local bar = AdminRankScroll:GetVBar()
		bar.Paint = function() end
		
		bar.btnUp.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 80 ) )
		end
		
		bar.btnDown.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 80 ) )
		end
		
		bar.btnGrip.Paint = function( s, w, h )
			draw.RoundedBox( 0, 0, 0, w, h, Color( 0, 0, 0, 120 ) )
		end
		
		local AdminRankLayout = vgui.Create( "DIconLayout", AdminRankScroll )
		AdminRankLayout:SetSize( AdminRankScroll:GetWide() - 10, AdminRankScroll:GetTall() )
		AdminRankLayout:SetPos( 5, 5 )
		AdminRankLayout:SetSpaceY( 4 )
		
		local function AddRank( n, rank, people )
			local UserButton = AdminRankLayout:Add( "DButton" )
			UserButton:SetSize( AdminRankLayout:GetWide(), 32 )
			UserButton:SetText( "" )
			UserButton:SetAlpha( 0 )
			UserButton:AlphaTo( 255, at * 2, n / 5 )
			UserButton.Paint = function( s, w, h )
				local col = currank == rank and cols.white or cols.lgray
				local bcol = currank == rank and cols.lgray or cols.rfgray
				draw.SimpleText( rank:UpperFirst(), "AdminRanks", w / 2, h / 2, col, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				draw.RoundedBox( 0, 0, h - 1, w, 1, bcol )
			end
			UserButton.DoClick = function()
				currank = rank
				curuser = 1
				table.Empty( curranktbl )
				
				for i = 1, #AdminRankList.Lines do
					AdminRankList:RemoveLine( i )
				end
				
				for id, info in pairs( data ) do
					if info.group == currank then
						table.insert( curranktbl, { name = info.name == nil and "Unknown" or info.name, steamid = id, group = info.group, allow = info.allow, deny = info.deny, firstjoin = info.firstjoin, lastjoin = info.lastjoin, totaltime = info.totaltime, totalkicks = info.totalkicks, totalbans = info.totalbans, timeperiod = info.timeperiod, timeperiodold = info.timeperiodold } )
						nline = AdminRankList:AddLine( info.name == nil and "Unknown" or info.name, id, info.group )
				
						for i = 1, 3 do
							nline.Columns[ i ]:SetTextColor( Color( 10, 10, 10 ) )
							nline.Columns[ i ]:SetFont( "AdminList" )
						end
					end
				end
				
				AdminStatsPersonAvatar:SetPlayer( util.SteamIDTo64( curranktbl[ curuser ].steamid ), 64 )
				
				for _, ply in pairs( player.GetAll() ) do
					if ply:SteamID() == curranktbl[ curuser ].steamid then
						curonline = true
						break
					else
						curonline = false
					end
				end
				
				AdminPermList:Clear()	
				
				for _, perm in pairs( rankperms[ currank ] ) do
					if type( perm ) == "table" then
						for _, perms in pairs( perm ) do
							AdminPermList:AddLine( perms )
						end
					end
				end
			end
			
			UserButton.DoRightClick = function()
				AdminRankPerms:SetVisible( true )
			end
		end
		
		for _, r in pairs( ranks ) do
			AddRank( _, r )
		end
		
	end )
end
net.Receive( "AdminStatsOpen", CreateAdminStats )
