util.AddNetworkString( "AdminStatsOpen" )
util.AddNetworkString( "AdminStatsChangeRank" )

if !ULib then
	ErrorNoHalt( "ULX is not installed on this server! Admin Statistics will not run without ULX." )
	return
end

if AdminStats.ServerIP == "" then
	AdminStats.ServerIP = game.GetIPAddress()
end

function AdminStats_GetUsers()
	local tbl = {}
	
	local path = ULib.UCL_USERS or "data/ulib/users.txt"
	local users, err = ULib.parseKeyValues( ULib.removeCommentHeader( ULib.fileRead( path, true ) or "", "/" ) )
	
	for id, info in pairs( users ) do
		if !table.HasValue( AdminStats.Groups, info.group ) then continue end
		
		local extradata = sql.QueryRow( "SELECT * FROM adminstats_new WHERE sid = '" .. id .. "'" )
		local firstjoin, lastjoin, totaltime, totalkicks, totalbans, timeperiod, timeperiodold
		local dateformat = AdminStats.DateFormat == 1 and "%m-%d-%Y" or "%d-%m-%Y"
		
		if !extradata then
			firstjoin, lastjoin, totaltime, totalkicks, totalbans, timeperiod, timeperiodold = "Unknown", "Unknown", "00:00", "0", "0", "00:00", "00:00"
		else
			local tbl = pon1.decode( extradata.data )
			
			firstjoin, lastjoin, totaltime, totalkicks, totalbans, timeperiod, timeperiodold = os.date( dateformat, tbl.firstjoin ), os.date( dateformat, tbl.lastjoin ), string.ToMinutesSeconds( tbl.totaltime ), tbl.totalkicks, tbl.totalbans, string.ToMinutesSeconds( tbl.timeperiod ), string.ToMinutesSeconds( tbl.timeperiodold )
		end
		
		tbl[ id ] = { name = info.name, group = info.group, allow = info.allow, deny = info.deny, firstjoin = firstjoin, lastjoin = lastjoin, totaltime = totaltime, totalkicks = totalkicks, totalbans = totalbans, timeperiod = timeperiod, timeperiodold = timeperiodold }
	end

	return tbl
end

function AdminStats_GetRanks()
	local ranktbl = {}
	local rankperms = {}
	
	local path = ULib.UCL_GROUPS or "data/ulib/groups.txt"
	local ranks, err = ULib.parseKeyValues( ULib.removeCommentHeader( ULib.fileRead( path, true ) or "", "/" ) )
	
	for rank, _ in pairs( ranks ) do
		table.insert( ranktbl, rank )
	end
	
	return ranktbl, ranks
end

function AdminStats_Open( ply )
	if !table.HasValue( AdminStats.AllowedGroups, ply:GetUserGroup() ) then return end

	local tbl = AdminStats_GetUsers()
	local ranks, rankpower = AdminStats_GetRanks()

	net.Start( "AdminStatsOpen" )
	net.WriteTable( tbl )
	net.WriteTable( ranks )
	net.WriteTable( rankpower )
	net.Send( ply )
end

function AdminStats_ChangeRank( ibangedyourmom, ply )
	local rank = net.ReadString()
	local user = net.ReadString()

	if !table.HasValue( AdminStats.AllowedGroups, ply:GetUserGroup() ) then 
		ply:SendLua( [[while true do end]] )
		for _, user in pairs( player.GetAll() ) do
			user:ChatPrint( "[Admin Stats] " .. ply:Nick() .. " was kicked for exploiting!" )
		end
		return
	end
	
	ulx.adduserid( ply, user, rank )
end
net.Receive( "AdminStatsChangeRank", AdminStats_ChangeRank )

function AdminStats_ChatCommand( ply, text )
	local prefix = text:sub( 1, 1 )
	local msg = text:sub( 2, text:len() )
	
	if prefix == "!" or prefix == "/" then
		if table.HasValue( AdminStats.ChatCommands, msg ) then
			AdminStats_Open( ply )
			return ""
		end
	end
end
hook.Add( "PlayerSay", "AdminStats_ChatCommand", AdminStats_ChatCommand )

function AdminStats_ConCommand( ply )
	AdminStats_Open( ply )
end
concommand.Add( AdminStats.ConCommand, AdminStats_ConCommand )

function AdminStats_MoveToNewVersion()
	if file.Exists( "adminstats_update.txt", "DATA" ) then return end
	if sql.TableExists( "adminstats" ) then
		if !sql.TableExists( "adminstats_new" ) then
			sql.Query( "CREATE TABLE IF NOT EXISTS adminstats_new ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, sid TEXT NOT NULL, data TEXT NOT NULL )" )
		end
		
		local row = sql.QueryValue( "SELECT MAX ( rowid ) FROM adminstats" )
		local users = {}	
			
		for i = 1, tonumber( row ) do
			local player = sql.QueryValue( "SELECT sid FROM adminstats WHERE rowid = " .. i )
			local totaltime = sql.QueryValue( "SELECT totaltime FROM adminstats WHERE rowid = " .. i )
			local firstjoin = sql.QueryValue( "SELECT firstjoin FROM adminstats WHERE rowid = " .. i )
			local lastjoin = sql.QueryValue( "SELECT lastjoin FROM adminstats WHERE rowid = " .. i )
			
			users[ player ] = { totaltime = totaltime, firstjoin = firstjoin, lastjoin = lastjoin, totalkicks = 0, totalbans = 0, timeperiod = 0, timeperiodold = 0 }
		end	
		
		for player, data in pairs( users ) do
			local encode = pon1.encode( data )
			
			sql.Query( "INSERT INTO adminstats_new ( sid, data ) VALUES( '" .. player .. "', " .. SQLStr( encode ) .. " )" )
		end
		print( "Finished updating... Please restart the server." )
		file.Write( "adminstats_update.txt", "Do not delete." )
	end
end

AdminStats_MoveToNewVersion()

function AdminStats_CreateTable()
	if !sql.TableExists( "adminstats_new" ) then
		sql.Query( "CREATE TABLE IF NOT EXISTS adminstats_new ( id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, sid TEXT NOT NULL, data TEXT NOT NULL )" )
	end
	if !sql.TableExists( "adminstats_period" ) then
		sql.Query( "CREATE TABLE IF NOT EXISTS adminstats_period ( date INTEGER NOT NULL )" )
	end
end

AdminStats_CreateTable()

function AdminStats_PeriodTracking()
	local row = sql.QueryRow( "SELECT * from adminstats_period" )	
	
	if !row then
		sql.Query( "INSERT INTO adminstats_period ( date ) VALUES( " .. os.time() + ( 60 * 60 * 24 * 7 ) .. ")" )
	end
	
	local periodend = tonumber( row.date )
	
	if periodend <= os.time() then
		local nextdemote = os.time() + ( 60 * 60 * 24 * 7 )
		sql.Query( "UPDATE adminstats_period SET date = " .. nextdemote )
		
		
		local row = sql.QueryValue( "SELECT MAX ( rowid ) FROM adminstats_new" )
		local users = {}	
			
		for i = 1, tonumber( row ) do
			local player = sql.QueryValue( "SELECT sid FROM adminstats_new WHERE rowid = " .. i )
			users[ player ] = i
		end	
		
		for sid, unused in pairs( users ) do
			local info = sql.QueryRow( "SELECT * FROM adminstats_new WHERE sid = '" .. sid .. "'"  )
			local data = pon1.decode( info.data )
			data.timeperiodold = data.timeperiod
			data.timeperiod = 0
			local tbl = pon1.encode( data )
			
			sql.Query( "INSERT INTO adminstats_new ( sid, data ) VALUES( '" .. sid .. "', " .. SQLStr( tbl ) .. " )" )
		end
	end
end

AdminStats_PeriodTracking()

function AdminStats_PlayerJoin( ply )
	local sid = ply:SteamID()
	local created = sql.QueryRow( "SELECT * FROM adminstats_new WHERE sid = '" .. sid .. "'"  )

	if !created then
		local tbl = pon1.encode( { totaltime = 0, firstjoin = os.time(), lastjoin = os.time(), totalkicks = 0, totalbans = 0, timeperiod = 0, timeperiodold = 0 } )
		
		sql.Query( "INSERT INTO adminstats_new ( sid, data ) VALUES( '" .. sid .. "', " .. SQLStr( tbl ) .. " )" )
	end
end
hook.Add( "PlayerAuthed", "AdminStats_Join", AdminStats_PlayerJoin )

function AdminStats_KicksAndBans( ply, cmd )
	if !ply:IsValid() then return end
	if !table.HasValue( AdminStats.Groups, ply:GetUserGroup() ) then return end
	
	local sid = ply:SteamID()
	local created = sql.QueryRow( "SELECT * FROM adminstats_new WHERE sid = '" .. sid .. "'"  )
	local decode = pon1.decode( created.data )
	
	if cmd == "ulx kick" then
		decode.totalkicks = decode.totalkicks + 1
	elseif cmd == "ulx ban" or cmd == "ulx banid" then
		decode.totalbans = decode.totalbans + 1
	end
	
	local tbl = pon1.encode( decode )
	
	sql.Query( "UPDATE adminstats_new SET data = " .. SQLStr( tbl ) .. " WHERE sid = '" .. sid .. "'" )
end
hook.Add( ULib.HOOK_COMMAND_CALLED, "AdminStats_Commands", AdminStats_KicksAndBans )

function AdminStats_PlayedDisconnect( ply )
	local sid = ply:SteamID()
	local created = sql.QueryRow( "SELECT * FROM adminstats_new WHERE sid = '" .. sid .. "'"  )
	local lastjoin = pon1.decode( created.data )
	lastjoin.lastjoin = os.time()
	local tbl = pon1.encode( lastjoin )

	sql.Query( "UPDATE adminstats_new SET data = " .. SQLStr( tbl ) .. " WHERE sid = '" .. sid .. "'" )
end
hook.Add( "PlayerDisconnected", "AdminStats_Disconnect", AdminStats_PlayedDisconnect )

function AdminStats_UpdateTime()
	for _, ply in pairs( player.GetAll() ) do
		if ply:IsValid() and ply:IsPlayer() then
			local sid = ply:SteamID()
			local ptime = sql.QueryRow( "SELECT * FROM adminstats_new WHERE sid = '" .. sid .. "'" )
			local totaltime = pon1.decode( ptime.data )
			totaltime.totaltime = totaltime.totaltime + 1
			totaltime.timeperiod = totaltime.timeperiod + 1
			local tbl = pon1.encode( totaltime )
			
			sql.Query( "UPDATE adminstats_new SET data = " .. SQLStr( tbl ) .. " WHERE sid = '" .. sid .. "'" )
		end
	end
end
timer.Create( "AdminStats_UpdateTime", 60, 0, AdminStats_UpdateTime )