AdminStats = AdminStats or {}

AdminStats.AllowedGroups = { "superadmin", "owner" } -- Groups that can view the menu

AdminStats.ChatCommands = { "admins" } -- Chat command name, DO NOT PUT A ! OR / IN FRONT, it will be done automatically.
AdminStats.ConCommand = "admin_stats" -- Console command name

AdminStats.Groups = { "owner", "superadmin", "headofstaff", "admin", "srmod", "mod", "tmod" } -- Groups to be tracked

AdminStats.AnimationTime = .4 -- Length for animations, initial amount

AdminStats.DateFormat = 2 -- Formatting for date; 1 = month/day/year, 2 = day/month/year

AdminStats.ServerIP = "89.34.96.39:27235" -- Fill in if you want the GameTracker portion to work more accurately